liblinux-inotify2-perl (1:2.3-2) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * debian/control: bumped Standards-Version to 4.6.1.
  * debian/copyright:
      - Converted the last paragraph of the license in a comment.
      - Updated packaging copyright years.
  * debian/tests/control: removed a useless CI test.

  [ Debian Janitor ]
  * Apply multi-arch hints. + liblinux-inotify2-perl: Drop Multi-Arch: same.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 25 Nov 2022 12:18:56 -0300

liblinux-inotify2-perl (1:2.3-1) unstable; urgency=medium

  * New upstream version 2.3.
  * Ran wrap-and-sort.
  * debian/control:
      - Bumped Standards-Version to 4.5.1.
      - Migrated DH level to 13.
  * debian/copyright:
      - Renamed license to Perl.
      - Updated upstream and packaging copyright years.
  * debian/patches/10_fix-interpreter-path.patch: added a leading zero to patch
    name.
  * debian/tests/control:
      - Added a new test.
      - Set the first test as superficial.
  * debian/upstream/metadata: created.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 16 Aug 2021 23:26:49 -0300

liblinux-inotify2-perl (1:2.2-2) unstable; urgency=medium

  * debian/control:
      - Bumped Standards-Version to 4.5.0.
      - Fixed a mistake. Moved 'Rules-Requires-Root: no' from binary to source
        stanza.
  * debian/copyright: updated upstream and packaging copyright years.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 26 Feb 2020 13:23:10 -0300

liblinux-inotify2-perl (1:2.2-1) unstable; urgency=medium

  * New upstream version 2.2.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.4.1.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 16 Dec 2019 12:18:08 -0300

liblinux-inotify2-perl (1:2.1-1) unstable; urgency=medium

  * New upstream version 2.1.
  * debian/control: added 'Multi-Arch: same' to liblinux-inotify2-perl.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 19 Nov 2018 20:19:26 -0200

liblinux-inotify2-perl (1:1.22-4) unstable; urgency=medium

  * Migrated DH level to 11.
  * debian/control:
      - Bumped Standards-Version to 4.2.1.
      - Changed Vcs-* URLs to salsa.debian.org.
  * debian/copyright:
      - Updated packaging copyright years.
      - Using a secure copyright format in URI.
  * debian/lintian-overrides: removed because it is not needed.
  * debian/tests/*: created to perform a trivial test.
  * debian/watch: using a secure URL for search.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 30 Sep 2018 00:48:56 -0300

liblinux-inotify2-perl (1:1.22-3) unstable; urgency=medium

  * debian/copyright: added a secondary email address for upstream.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 12 Nov 2016 17:16:28 -0200

liblinux-inotify2-perl (1:1.22-2) unstable; urgency=medium

  * Bumped DH level to 10.
  * debian/control:
      - Added the Perl module name to long description.
      - Bumped Standards-Version to 3.9.8.
      - Updated the Vcs-* fields to use https instead of http and git.
  * debian/copyright: updated some information about packaging.
  * debian/gbp.conf: no longer used by me... Removed.
  * debian/lintian-overrides: added to override a false positive about GCC
    hardening.
  * debian/patches/eg: renamed to 10_fix-interpreter-path.patch.
  * debian/rules:
      - Added the DEB_BUILD_MAINT_OPTIONS variable to improve the GCC hardening.
      - Removed the --parallel option from dh.
  * debian/watch: bumped to version 4.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 10 Oct 2016 09:12:23 -0300

liblinux-inotify2-perl (1:1.22-1) unstable; urgency=medium

  * New maintainer. Thanks a lot to Michael Bramer, the initial maintainer,
    for your nice work over this package. (Closes: #753547, #752852)
  * Ack NMU bugs:
      - Non-maintainer upload for the Perl 5.10 transition. Don't try to
        remove /usr/share/perl5 if it doesn't exist. (Closes: #463459) [1]
      - New upstream release. (Closes: #568423) [2]
      - Updated debhelper compatibility from 4 to 5. [2]
      - Only build on Linux-based architectures. [3]
      - New upstream version. (Closes: #639238) [3]
      - Thanks to:
          [1] Niko Tyni <ntyni@debian.org>
          [2] Franck Joncourt <franck@debian.org>
          [3] Hilko Bengen <bengen@debian.org>
  * Migrations:
      - debian format from 1.0 to 3.0.
      - debian/copyright to new format (1.0).
      - debian/rules: to new (reduced) format.
      - debhelper version from 5 to 9.
      - Standards-Version from 3.7.2 to 3.9.5.
  * debian/control:
      - Added libcommon-sense-perl in Build-Depends field.
      - Added the Homepage field.
      - Added the VCS fields.
      - Improved the short and long description.
      - Removed unusual extra leading space from short description.
  * debian/examples: added to install eg/* files.
  * debian/gbp.conf: added to allow git-buildpackage usage.
  * debian/patches/: added the eg patch to fix the interpreter path.
  * debian/watch: upgraded from version 2 to 3 and improved.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 03 Jul 2014 09:58:27 -0300

liblinux-inotify2-perl (1:1.22-0.2) unstable; urgency=low

  * Non-maintainer upload.
  * Only build on Linux-based architectures.

 -- Hilko Bengen <bengen@debian.org>  Thu, 25 Aug 2011 12:26:04 +0200

liblinux-inotify2-perl (1:1.22-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream version (Closes: #639238)

 -- Hilko Bengen <bengen@debian.org>  Thu, 25 Aug 2011 11:41:48 +0200

liblinux-inotify2-perl (1:1.2-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream release. (Closes: #568423)
  * Updated debhelper compatibility from 4 to 5.

 -- Franck Joncourt <franck@debian.org>  Tue, 23 Mar 2010 20:47:14 +0100

liblinux-inotify2-perl (1:1.1-2.1) unstable; urgency=low

  * Non-maintainer upload for the Perl 5.10 transition.
  * Don't try to remove /usr/share/perl5 if it doesn't exist. (Closes: #463459)

 -- Niko Tyni <ntyni@debian.org>  Wed, 02 Apr 2008 22:09:37 +0300

liblinux-inotify2-perl (1:1.1-2) unstable; urgency=low

  * Thanks to LaMont Jones:
     * Work on all architectures that define the syscalls

 -- Michael Bramer <grisu@debian.org>  Wed, 29 Aug 2007 13:41:51 +0200

liblinux-inotify2-perl (1:1.1-1) unstable; urgency=low

  * upgrade (closes: #416329, #382417)

 -- Michael Bramer <grisu@debian.org>  Fri, 22 Jun 2007 21:25:10 +0100

liblinux-inotify2-perl (1.01-2) unstable; urgency=low

  * now with mips and mipsel support (closes: #358957)

 -- Michael Bramer <grisu@debian.org>  Sun, 26 Mar 2006 00:08:57 +0100

liblinux-inotify2-perl (1.01-1) unstable; urgency=low

  * Initial Release.

 -- Michael Bramer <grisu@debian.org>  Mon,  6 Mar 2006 14:15:30 +0100
